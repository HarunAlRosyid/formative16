package com.nexsoft.arisan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.nexsoft.arisan.controller.ArisanController;
import com.nexsoft.arisan.model.LotteryModel;
import com.nexsoft.arisan.model.MemberModel;
import com.nexsoft.arisan.view.ArisanView;

@SpringBootApplication
public class ArisanApplication {	
	public static void main(String[] args) {
//		SpringApplication.run(ArisanApplication.class, args);
		ArisanView view= new ArisanView();
		MemberModel member= new MemberModel();
		ArisanController controller= new ArisanController(member,view);
		
		controller.chooseMenu();

	}
}
