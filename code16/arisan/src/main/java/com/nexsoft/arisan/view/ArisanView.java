package com.nexsoft.arisan.view;

import java.util.ArrayList;
import java.util.Locale;
import java.text.NumberFormat;
import com.nexsoft.arisan.model.MemberModel;


public class ArisanView {
	
	public String currencyID(double nominal){
	    Locale localeID = new Locale("in", "ID");
	    NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
	    return formatRupiah.format((double)nominal);
	}
	
	public void listMenu() {
		String[] menu = {"Tambah Peserta", "Lihat Daftar Peserta", "Undi Pemenang", "Keluar"};
        
        System.out.println("-----------Arisan App-----------");
        
        for (int i = 0; i < menu.length; i++) {
            System.out.println(i + 1 + "." + menu[i]);
        }
        breakln();
        System.out.println("Pilih Menu");
	}

	public void formAdd() {
		System.out.println("Tambah Peserta");
		System.out.println("Masukan Data Peserta");
		System.out.println("Nama :");
	}
	
	public void printAll(ArrayList<MemberModel> dataMember) {		
		breakln();
		System.out.println("Daftar Peserta");
		for (int i = 0; i < dataMember.size(); i++) {
			double amount = Double.valueOf(dataMember.get(i).getReceived());
			String output = currencyID(amount);
			
			System.out.println(
                "Id : " + dataMember.get(i).getId() +
                ", Nama : " + dataMember.get(i).getName() +
                ", Nomer Member : " + dataMember.get(i).getNumber() +
                "\nPemenang pertemuan ke : " + dataMember.get(i).getWinOrder() +
                ", Uang diterima : " + output);
                
			breakln();
        }
	}

	public void meet() {
		breakln();
		System.out.println("Mengundi Pemenang Arisan");
	}

	public void breakln() {
        System.out.println("--------------------------------");
	}
 
}
