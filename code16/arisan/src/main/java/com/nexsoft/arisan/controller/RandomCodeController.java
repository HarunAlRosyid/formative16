package com.nexsoft.arisan.controller;

import java.util.ArrayList;
import java.util.Random;

import org.apache.commons.rng.UniformRandomProvider;
import org.apache.commons.rng.simple.RandomSource;

import com.nexsoft.arisan.model.LotteryModel;


public class RandomCodeController {
	
	private final UniformRandomProvider rng = RandomSource.MT_64.create();
	
	ArrayList<LotteryModel> dataLottery = new ArrayList<LotteryModel>();
	
	
	public void dataLottery(LotteryModel data) {
        this.dataLottery.add(data);
    }
		
	public String randomGenerator() {
		String alphabetNumeric = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890abcdefghijklmnopqrstuvwxyz";
		
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		int length = 10;
		for(int i = 0; i < length; i++) {
			int index = random.nextInt(alphabetNumeric.length());
		    char randomChar = alphabetNumeric.charAt(index);
		    sb.append(randomChar);
		}
	
		String randomString = sb.toString();
		return randomString;
	}
	
	public String getRandomLotteryCode() {
		return dataLottery.get(randomLottery()).getLotteryCode();
	}
	
		
	public int randomLottery() {
		int lotteryWin = rng.nextInt(dataLottery.size());
		return lotteryWin;
	}
	
	public void remove() {
		dataLottery.remove(randomLottery());
	}
	
	public boolean lotteryIsEmpty(){
        return dataLottery.isEmpty();
    }
	public boolean lotteryMaximum(){
        return dataLottery.size()>15;
    }
}
